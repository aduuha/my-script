#!/bin/bash

# Обновляем список пакетов
apt-get update

# Проверяем наличие обновлений для package2
if apt-cache policy package2 | grep -q "Installed: (none)"; then
    echo "package2 is not installed"
else
    echo "package2 is already installed"
fi

if apt-cache policy package2 | grep -q "Candidate: "; then
    candidate=$(apt-cache policy package2 | grep "Candidate: " | awk '{print $2}')
    installed=$(dpkg -s package2 | grep "^Version:" | awk '{print $2}')
    if [ "$candidate" != "$installed" ]; then
        echo "Installing package2 version $candidate"
        apt-get install -y package2
    else
        echo "package2 is up to date"
    fi
fi
