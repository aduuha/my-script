#!/bin/bash
timeout=6
for i in $(cat urls.txt)
    do
        if curl -I --silent --fail --max-time "$timeout" "{$i}" 2>&1 | grep "HTTP/.*"; then
        echo "$i доступен"
    else
        echo "$i недоступен"
    fi
done
